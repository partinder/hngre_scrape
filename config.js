/**
 * Created by Partinder on 5/6/15.
 */

module.exports = {

    proxy_list:[],
    proxy_list_length : Number,
    current_proxy_url:String, // proxy URL
    current_proxy_index: 0, // Index of proxy url in proxy list
    restaurant_yelp_links:[],
    myip : String,
    page_index : 0,
    yelp_link_index : 0,
    current_page : 0

}