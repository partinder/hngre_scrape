/**
 * Created by Partinder on 5/7/15.
 */
var mongoose = require("mongoose")

var reviewsSchema = new mongoose.Schema({
    reviewer_name : String,
    reviewer_location : String,
    review_count : Number,
    review_link : String,
    review_rating : String,
    review : String,
    yelp_link : String

})

var reviews = mongoose.model("Reviews",reviewsSchema)

module.exports.reviews = reviews
module.exports.reviewsSchema = reviewsSchema
