/**
 * Created by Partinder on 5/6/15.
 */

var helpers = require("./helpers")
var config = require("./config")
var scrapper = require("./scrapper")



module.exports = function(){

    helpers.asyncLoop(config.proxy_list_length, function(loop_proxy) {

            console.log("Testing for Proxy : %s",config.proxy_list[loop_proxy.iteration()])

            helpers.findAnoProxy(config.myip,loop_proxy.iteration(),function(err) {

                if(err)
                {
                    console.log("Status : %s",err)
                    loop_proxy.next()
                }
                else
                {
                    console.log("Proxy good to be used")
                    //Start Scraping
                    helpers.asyncLoopProxy(config.restaurant_yelp_links.length,function(loop_yelp){

                        console.log("Yelp link to be Scarapped: %s",config.restaurant_yelp_links[config.yelp_link_index+loop_yelp.iteration()])


                        scrapper.yelp_scrape(config.yelp_link_index+loop_yelp.iteration(),config.page_index,loop_proxy.iteration(), function(err,start_page){

                            if(err)
                            {
                                config.page_index = start_page
                                loop_yelp.break(err,config.yelp_link_index+loop_yelp.iteration())

                            }
                            else
                            {
                                config.page_index = 0
                                config.current_page = 0
                                console.log("Scrapping done for %s, restaurants",(config.yelp_link_index+loop_yelp.iteration()+1))
                                console.log("Next Restaurant")
                                loop_yelp.next()
                            }

                        })

                    },function(err,yelp_index){

                        if(!err && !yelp_index)
                        {
                            console.log("All Scrapping Done")
                        }
                        else
                        {
                            console.log("Trying Another Proxy")
                            config.yelp_link_index = yelp_index
                            console.log("Yelp Index: %s", config.yelp_link_index)
                            loop_proxy.next()
                        }

                        //console.log("All Yelp done")

                    })

                }
            })
        },function()
        {
            console.log('All Proxies Consumed')
        }
    )
}


