/* * Created by Partinder on 5/7/15 */
var request = require("request")
var cheerio = require("cheerio")
var config = require("./config")
var helpers = require("./helpers")



module.exports ={

    yelp_scrape : function(yelplist_index,page_index,proxy_index,callback){

        console.log("Starting Scrapping for : %s", config.restaurant_yelp_links[yelplist_index])

        var proxy_options = {
            "url": config.restaurant_yelp_links[yelplist_index]+"?sort_by=elites_desc",
            "proxy": "http://" + config.proxy_list[proxy_index],
            "timeout": 3000000
        }

        request(proxy_options, function(err,res,body){
            console.log("received reply from Yelp")

            if(!err && (res.statusCode == 200 || res.statusCode == 201))
            {
                console.log("Received Body")
                var $ = cheerio.load(body)
                var review_count = $("div[class='page-of-pages arrange_unit arrange_unit--fill']").text()
                console.log(review_count)
                review_count = review_count.split(" ")
                console.log(review_count[11])
                var no_pages = review_count[11]
                no_pages = no_pages.split("'\'")
                no_pages = no_pages[0]
                //if(review_count > 500)
                //{
                //    no_pages = 13
                //}

                helpers.asyncLoopYelp((no_pages-(page_index/40)),function(loop_scrape){

                    var start_page = page_index+((loop_scrape.iteration())*40)
                    config.current_page = start_page

                    var scrape_options = {
                        "url": config.restaurant_yelp_links[yelplist_index]+"?sort_by=elites_desc&start="+start_page,
                        "proxy": "http://" + config.proxy_list[proxy_index],
                        "timeout": 3000000
                    }

                    console.log("Starting to Scrape Page: %s",start_page)

                    request(scrape_options, function(err,res,body){

                        if(res)
                        {
                            if(!err && (res.statusCode == 200 || res.statusCode == 201))
                            {
                                //console.log("Load Cheerio")

                                var $ = cheerio.load(body)

                                var reviews = $("div[class=review-list] > ul >li")

                                //#super-container > div > div > div.column.column-alpha.main-section > div:nth-child(3) > div.feed > div.review-list > ul > li:nth-child(1)

                                //console.log(reviews.count)
                                reviews.each(function(){

                                    var user_review = {}

                                    var id = $(this).find("div > div.review-sidebar > div > div > div.media-story > ul.user-passport-info > li.user-name > a").attr("href")
                                    id = id.split("=")

                                    user_review.reviewer_id = id[1]
                                    user_review.reviewer_name = $(this).find("div > div.review-sidebar > div > div > div.media-story > ul.user-passport-info > li.user-name > a").text()
                                    user_review.reviewer_photo = $(this).find("div > div.review-sidebar > div > div > div.media-avatar > div > a > img").attr("src")
                                    user_review.reviewer_location = $(this).find("div > div.review-sidebar > div > div > div.media-story > ul.user-passport-info > li.user-location > b").text()
                                    user_review.reviewer_count = $(this).find("div > div.review-sidebar > div > div > div.media-story > ul.user-passport-stats > li.review-count > span > b").text()
                                    user_review.reviewer_friend_count = $(this).find("div > div.review-sidebar > div > div > div.media-story > ul.user-passport-stats > li.friend-count > span > b").text()
                                    user_review.review_link = $(this).find("div > div.review-sidebar > div > ul > li:nth-child(1) > a").attr("data-pop-uri")
                                    user_review.review_rating = $(this).find("div > div.review-wrapper > div.review-content > div.biz-rating.biz-rating-very-large.clearfix > div > div > meta").attr("content")
                                    user_review.review = $(this).find("div > div.review-wrapper > div.review-content > p").text()
                                    //user_review.restaurant_review_count = review_count
                                    user_review.yelp_link = config.restaurant_yelp_links[yelplist_index]


                                    //console.log("Saving to MOngo: %s",user_review.reviewer_id)
                                    helpers.savetomongoose(user_review, function(err){
                                        if(err)
                                        {
                                            console.log(err)
                                        }
                                        else
                                        {

                                        }
                                    })

                                })


                                console.log("Done Page: %s",start_page)
                                loop_scrape.next()
                                //callback(null)

                            }
                            else
                            {
                                if(err)
                                {
                                    console.log(err)
                                    loop_scrape.break(err,start_page)
                                    //callback(err,start_page)
                                }
                                else
                                {
                                    console.log(res.statusCode)
                                    loop_scrape.break(res.statusCode,start_page)
                                    //callback(err,start_page)
                                }

                            }

                        }
                        else
                        {
                            if(err)
                            {
                                console.log(err)
                                loop_scrape.break(err,start_page)
                                //callback(err,start_page)
                            }
                            else
                            {
                                //console.log(res.statusCode)
                                //loop_scrape.index -= 1
                                //loop_scrape.next()
                                loop_scrape.break("No Resposnse",start_page)
                                //callback(err,start_page)
                            }
                        }


                    })


                },function(err,start_page){

                    if(!err & !start_page)
                    {
                        console.log("Done Scraping : %s",config.restaurant_yelp_links[yelplist_index])
                        helpers.yelpdone(config.restaurant_yelp_links[yelplist_index], function(err){
                            if(err)
                            {

                            }
                            callback(err,0)

                        })
                        //callback(null,null)


                    }
                    else
                    {
                        callback(err,start_page)
                    }



                })


            }
            else
            {
                if(res)
                {
                    console.log("Err from Yelp, %s", res.statusCode)
                    if(err)
                    {
                        callback(err,config.current_page)
                    }
                    else
                    {
                        callback(res.statusCode,config.current_page)
                    }
                }
                else
                {
                    console.log("No Response")
                    callback("No Response",config.current_page)
                }


            }
        })


    }
}
