/**
 * Created by Partinder on 5/5/15.
 */
var request = require("request")
var async = require("async")
var config = require("./config")
var MongoClient = require('mongodb').MongoClient;
var mongoose = require("mongoose")
var db



//MongoClient.connect("mongodb://localhost/db_hngre",function(err,mongodb) {
//    if (err) {
//        console.log(err)
//    }
//    else {
//        db=mongodb
//        console.log("DB Connected")
//    }
//})

MongoClient.connect("mongodb://52.5.0.16/db_hngre",function(err,mongodb) {
    if (err) {
        console.log(err)
    }
    else {
        db=mongodb
        console.log("DB Connected")
    }
})




module.exports = {

    getproxies: function (callbackM) {
        var proxies = []
        var proxylist = [
            "http://proxy-ip-list.com/download/free-proxy-list.txt",
            "http://proxy-ip-list.com/download/free-uk-proxy-list.txt",
            "http://proxy-ip-list.com/download/free-usa-proxy-ip.txt",
            "http://proxy-ip-list.com/download/proxy-list-port-3128.txt",
            "http://txt.proxyspy.net/proxy.txt"
        ]

        var queries = []

        proxylist.forEach(function (proxy) {

            queries.push(
                function (callback) {

                    request(proxy, function (err, res, body) {

                        if(res)
                        {
                            if(!err & (res.statusCode == 200 || res.statusCode == 201))
                            {
                                callback(null, body)
                            }

                        }

                        else
                        {
                            callback(null,null)
                        }


                    })

                }
            )
        })


        async.parallel(queries, function (err, results) {

            if (err) {
                callbackM(err, null)
            }


            for (data = 0; data < results.length; data++)
            {

                if(results[data])
                {
                    var lines = results[data].split("\n")
                    lines.forEach(function (line) {
                        //console.log(line)
                        if (line.startsWith("#") || line.length <= 5 || line.startsWith("Proxy list") || line.startsWith("IP address") || line.startsWith("Free")) {

                        }
                        else {
                            //console.log(line)
                            var split = line.split(";");
                            if (split[0].indexOf(" ")) {
                                split = split[0].split(" ");
                            }
                            proxies.push(split[0]);

                        }


                    })

                }


            }
            //console.log(proxies)

            config.proxy_list = proxies
            config.proxy_list_length = proxies.length
            getMyIp(function(err){
                if(err)
                {
                    callbackM(err)
                }
                else
                {
                    callbackM(null, proxies)
                }


            })

        })

    },
    findAnoProxy: function (myip, proxy_index, callback) {


        var proxyoptions = {
            "url": 'https://wtfismyip.com/json',
            "proxy": "http://" + config.proxy_list[proxy_index],
            "timeout": 15000
        }

        request(proxyoptions, function (err, res, body) {

            if (!err && res.statusCode == 200) {
                try
                {
                    var myproxyip = JSON.parse(body)["YourFuckingIPAddress"]
                }
                catch (err)
                {
                    console.log(body)
                    console.log("Proxy Error,%s", err)
                    return callback(err)
                }


                console.log("My Ip: "+myip)
                console.log("Proxy Ip: "+myproxyip)

                if (myip != myproxyip)
                {
                    callback()
                    // Test Headers
                    //testHeaders(proxy_index, myip, function (err) {
                    //
                    //    if (err) {
                    //        callback(err)
                    //    }
                    //
                    //    else {
                    //        callback(null)
                    //    }
                    //})
                }
                else {

                    callback("Proxy not Anonymous")
                }


            }
            else {
                if (err) {
                    callback(err)
                    //console.log(err)
                }
                else {
                    //console.log("Proxy Response Not Valid %s",res.statusCode)
                    callback("Response not Valid")
                }

            }

        })


    },
    getYelplinks: function (callback) {

        var merchant = db.collection('yelplinks')
        merchant.distinct('link', function (err, links) {

                    if (err) {
                        callback(err, null)
                    }
                    else {
                        config.restaurant_yelp_links = links
                        callback(null, links)
                    }
                    //Links
                })


    },
    asyncLoop: function (iterations, func, callback) {
    var index = 0;
    var done = false;
    var loop = {
        next: function() {
            if (done) {
                return;
            }

            if (index < iterations) {
                index++;
                func(loop);

            } else {
                done = true;
                callback();
            }
        },

        iteration: function() {
            return index - 1;
        },

        break: function() {
            done = true;
            callback();
        }
    };
    loop.next();
    return loop;
},
    asyncLoopProxy: function (iterations, func, callback) {
        var index = 0;
        var done = false;
        var loop = {
            next: function() {
                if (done) {
                    return;
                }

                if (index < iterations) {
                    index++;
                    func(loop);

                } else {
                    done = true;
                    callback(null,null);
                }
            },

            iteration: function() {
                return index - 1;
            },

            break: function(err,yelp_index) {
                done = true;
                callback(err,yelp_index);
            }
        };
        loop.next();
        return loop;
    },
    asyncLoopYelp: function (iterations, func, callback) {
        var index = 0;
        var done = false;
        var loop = {
            next: function(val) {
                if (done) {
                    return;
                }

                if (index < iterations) {
                    index ++
                    func(loop);

                } else {
                    done = true;
                    callback(null,null);
                }
            },

            iteration: function() {
                return index - 1;
            },

            break: function(err,page_index) {
                done = true;
                callback(err,page_index);
            }
        };
        loop.next();
        return loop;
    },
    yelpdone: function(yelp_link,callback){

        var yelplinks = db.collection("yelplinks")
        yelplinks.deleteOne({link:yelp_link},function(err,res){
                    if(err)
                    {
                        callback(err)
                    }
                    else
                    {
                        callback(null)
                    }
                })


},
    savetomongoose: function(review,callback){

         var reviews = db.collection("reviews")
                reviews.insert(review, function(err,res){
                    if(err)
                    {
                        console.log(err)
                        callback(err)
                    }
                    else
                    {
                        //console.log(review.reviewer_id)

                        callback()
                    }
                })
            }





}

if ( typeof String.prototype.startsWith != 'function' )
{
    String.prototype.startsWith = function( str )
    {
        return this.substring( 0, str.length ) === str;
    }
}

function testHeaders(proxyIndex,myip,callback){

    var proxyoptions ={
        "url" : 'https://wtfismyip.com/headers',
        "proxy": "http://"+config.proxy_list[proxyIndex],
        "timeout": 15000
    }
    request(proxyoptions,function(err,res,body){

        if(!err && (res.statusCode == 200 || res.statusCode == 201))
        {
            var myproxyheader = body.toString()
            if(myproxyheader.indexOf(myip) == -1)
            {
                //Proxy Good to be used
                callback(null)

            }
            else
            {
                callback("Headers Not hiding IP")
            }
        }
        else
        {
            if(err)
            {
                callback(err)
            }
            else
            {
                callback("Not Valid Response, code: "+res.statusCode)
            }

        }

    })

}
function getMyIp(callback) {

    request('https://wtfismyip.com/json', function (err, res, body) {
        if (!err && res.statusCode == 200)
        {
            config.myip = JSON.parse(body)["YourFuckingIPAddress"]
            callback(null)
        }
        else
        {
            if(err)
            {
                callback(null)
            }
            else
            {
                callback("Not Valid Response, code: "+res.statusCode)
            }
        }
    })
}





